﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	public int soundTrack;
	public AudioClip[] PlayList;


	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}

	// Use this for initialization
	void Start () {
		audio.clip = PlayList[2];
		if (!audio.isPlaying && audio.clip.isReadyToPlay)
			audio.Play();
	}

	void trackChange(){
		// update the change in track.
//		audio.Play(); 
		audio.clip = PlayList[soundTrack];
//		audio.Play;
	}

	// Update is called once per frame
	void Update () {
		if (!audio.isPlaying && audio.clip.isReadyToPlay)
			audio.Play();
	}
	void Winner(){
		//Play Victory End
		audio.clip = PlayList[4];
//		audio.Play;
		}

	void Defeat(){
		//Play Failure End
		audio.clip = PlayList[3];
//		audio.Play;
		}
	void Loop (){
		//Play Flying Loop
		audio.clip = PlayList[1];
//		audio.Play;
		}
	void Combat (){
		//Play Combat Loop
		audio.clip = PlayList[2];
//		audio.Play;
		}
	void MainMenu(){
		//Play Title Theme loop
		audio.clip = PlayList[0];
//		audio.Play;
		}
}

//Title Theme (Wave Loop)		1:01			6/9/14, 12:20 PM			
//Victory (Wave Loop)		0:10			6/9/14, 12:20 PM			3
//	Flying (Wave Loop)		0:20			6/9/14, 12:19 PM			1
//		Failure (Wave Loop)		0:10			6/9/14, 12:19 PM			3
//		Combat (Wave Loop)		0:20			6/9/14, 12:19 PM			3
