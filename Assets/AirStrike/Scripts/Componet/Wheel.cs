﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	public Vector3 Axis = Vector3.one;
	public float Torque= 100F;
	void Start () {
	
	}
	
	private void FixedUpdate ()
	{
		this.transform.Rotate(Axis * Torque *Time.fixedDeltaTime);
	}
}
