﻿using UnityEngine;
using System.Collections;

public class Aerodrome : MonoBehaviour {
	public int index =0;
	public GameObject[] aircraft;
	public GUISkin mySkin2;
	public int Mode;
	public GUISkin skin;
	public Texture2D Logo;

	// Use this for initialization
	void Start () {
		index = PlayerPrefs.GetInt("myAircraft");
		if (index == null){
			index = 0;
		}
		aircraft[index].SetActiveRecursively(true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void OnGUI ()
	{
		if(skin)
			GUI.skin = skin;
		
		if (GUI.Button(new Rect(70,10,120,30),"Switch Aircraft"))
		{
			index = index+1;
//			if(index == aircraft.length){
//				index = 0;
//			}
			aircraft[0].SetActiveRecursively(false);
			aircraft[1].SetActiveRecursively(false);
			aircraft[2].SetActiveRecursively(false);
			aircraft[3].SetActiveRecursively(false);
			aircraft[4].SetActiveRecursively(false);
			aircraft[5].SetActiveRecursively(false);
			aircraft[6].SetActiveRecursively(false);
			aircraft[7].SetActiveRecursively(false);
			aircraft[8].SetActiveRecursively(false);
			aircraft[9].SetActiveRecursively(false);
			aircraft[10].SetActiveRecursively(false);
			aircraft[11].SetActiveRecursively(false);
//			aircraft[12].SetActiveRecursively(false);
			aircraft[index].SetActiveRecursively(true);

		}
		if (GUI.Button (new Rect(10,10,100,30),"Back"))
		{
			PlayerPrefs.SetInt("myAircraft",index);
			Application.LoadLevel("MainMenu");
		}
		
	}
}



