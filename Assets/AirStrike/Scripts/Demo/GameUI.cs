﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour
{

	public GUISkin skin;
	public Texture2D Logo;
	public Texture2D ControlsIMG;
	public int Mode;
	private int screenWidth;
	private int screenHeight;
	public int buttonHeight;
	
	void Start () {
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		double fButton = Screen.height * 0.08;
		buttonHeight = (int)fButton;
	}
	
	void Update ()
	{
	
	}
	
	public void OnGUI ()
	{
		GameManager game = (GameManager)GameObject.FindObjectOfType (typeof(GameManager));
		PlayerController play = (PlayerController)GameObject.FindObjectOfType (typeof(PlayerController));
		
		if (skin)
			GUI.skin = skin;
		
		
		switch (Mode) {
		case 0:
			if (Input.GetKeyDown (KeyCode.Escape)) {
				Mode = 2;	
			}
			
			if (play) {
				WeaponController weapon = play.GetComponent<WeaponController> ();
				play.Active = true;
			
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUI.Label (new Rect (20, 20, 200, buttonHeight), "Killed " + game.Killed.ToString ());
				GUI.Label (new Rect (20, 50, 200, buttonHeight), "Score " + game.Score.ToString ());
			
			
				GUI.skin.label.alignment = TextAnchor.UpperRight;
				GUI.Label (new Rect (Screen.width - 220, 20, 200, buttonHeight), "ARMOR " + play.GetComponent<DamageManager> ().HP);
				if (weapon.WeaponLists [weapon.CurrentWeapon].Icon)
					GUI.DrawTexture (new Rect (Screen.width - 100, Screen.height - 100, 80, 80), weapon.WeaponLists [weapon.CurrentWeapon].Icon);
				
				GUI.skin.label.alignment = TextAnchor.UpperRight;
				if (weapon.WeaponLists [weapon.CurrentWeapon].Ammo <= 0 && weapon.WeaponLists [weapon.CurrentWeapon].ReloadingProcess > 0) {
					if (!weapon.WeaponLists [weapon.CurrentWeapon].InfinityAmmo)
						GUI.Label (new Rect (Screen.width - 230, Screen.height - 120, 200, buttonHeight), "Reloading " + Mathf.Floor ((1 - weapon.WeaponLists [weapon.CurrentWeapon].ReloadingProcess) * 100) + "%");
				} else {
					if (!weapon.WeaponLists [weapon.CurrentWeapon].InfinityAmmo)
						GUI.Label (new Rect (Screen.width - 230, Screen.height - 120, 200, buttonHeight), weapon.WeaponLists [weapon.CurrentWeapon].Ammo.ToString ());
				}
				
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUI.Label (new Rect (20, Screen.height - 50, 250, buttonHeight), "R : Switch Guns C : Change Camera");
			
			}
			break;
		case 1:
			if (play)
				play.Active = false;
			
			Screen.lockCursor = false;
			
			GUI.skin.label.alignment = TextAnchor.MiddleCenter;
			GUI.Label (new Rect (0, Screen.height / 2 + buttonHeight*3, Screen.width, buttonHeight), "Game Over");
		
			GUI.DrawTexture (new Rect (Screen.width / 2 - Logo.width / 2, Screen.height / 2 - 150, Logo.width, Logo.height), Logo);
		
			if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height / 2 + buttonHeight, 300, buttonHeight), "Restart")) {
				Application.LoadLevel (Application.loadedLevelName);
			
			}
			if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height / 2 + buttonHeight*2, 300, buttonHeight), "Main menu")) {
				Application.LoadLevel ("Mainmenu");
			}
			break;
		
		case 2:
			if (play)
				play.Active = false;
			
			Screen.lockCursor = false;
			Time.timeScale = 0;
			GUI.skin.label.alignment = TextAnchor.MiddleCenter;
			GUI.Label (new Rect (0, Screen.height / 2 + 10, Screen.width, 30), "Pause");
		
			GUI.DrawTexture (new Rect (Screen.width / 2 - Logo.width / 2, Screen.height / 2 - 150, Logo.width, Logo.height), Logo);
		
			if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height / 2 + buttonHeight, 300, buttonHeight), "Resume")) {
				Mode = 0;
				Time.timeScale = 1;
			}
			if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height / 2 + buttonHeight*2, 300, buttonHeight), "Main menu")) {
				Time.timeScale = 1;
				Mode = 0;
				Application.LoadLevel ("Mainmenu");
			}
			if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height / 2 + buttonHeight*3 , 300, buttonHeight), "Controls")) {
				Time.timeScale = 1;
				Mode = 3;
			}
			break;

		 case 3:
			GUI.DrawTexture (new Rect (Screen.width / 2 - ControlsIMG.width / 2, Screen.height / 2 - 150, ControlsIMG.width, ControlsIMG.height), ControlsIMG);
			Time.timeScale = 0;

			if (GUI.Button (new Rect (Screen.width / 2 - 150, buttonHeight+10, 300, buttonHeight), "Resume")) {
				Mode = 0;
				Time.timeScale = 1;
			}
			break;	
		}
	}
}
