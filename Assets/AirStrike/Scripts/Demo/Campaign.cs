﻿using UnityEngine;
using System.Collections;

public class Campaign : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GUISkin skin;
	
	public Texture2D Logo;
	public Texture2D Logo2;
	public int Mode;
	
	public void OnGUI ()
	{
		if(skin)
			GUI.skin = skin;
		
		//GUI.DrawTexture(new Rect(Screen.width/2 - Logo.width/2 , Screen.height/2 - 165,Logo.width,Logo.height),Logo);
		GUI.Label (new Rect (Screen.width / 2 - Logo.width / 2, Screen.height / 2 - (Logo.height + 5), 200, 50), "Choose your Side:");

		if(GUI.Button(new Rect(Screen.width/2 - Logo.width/2-(Logo.width-5), Screen.height/2 - 165,Logo.width,Logo.height),Logo)){
			PlayerPrefs.SetString("Side","cross");
			Application.LoadLevel("Campaign_Cross");
		}
		if(GUI.Button(new Rect(Screen.width/2 - Logo2.width/2+(Logo2.width+5), Screen.height/2 - 165,Logo2.width,Logo2.height),Logo2)){

			PlayerPrefs.SetString("Side","crockade");
			Application.LoadLevel("Campaign_Cross");
		}
		
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		//GUI.Label(new Rect(0,Screen.height-90,Screen.width,50),"Air strike starter kit beta. by Rachan Neamprasert\n www.hardworkerstudio.com");
		
	}

}