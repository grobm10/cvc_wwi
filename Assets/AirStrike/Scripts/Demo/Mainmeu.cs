﻿using UnityEngine;
using System.Collections;

public class Mainmeu : MonoBehaviour {

	public GUISkin skin;
	public Texture2D Logo;
	private int screenWidth;
	private int screenHeight;
	public int buttonHeight;

	void Awake () {
				// Initialize FB SDK              
//				enabled = false;                  
//				FB.Init (SetInit, OnHideUnity);  
		}

	void Start () {
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		double fButton = Screen.height * 0.08;
		buttonHeight = (int)fButton;
		Screen.showCursor = true;
		Screen.lockCursor = false;
//		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void Update () {
	
	}
	
	public void OnGUI(){
		if(skin)
		GUI.skin = skin;
		
		GUI.DrawTexture(new Rect(Screen.width/2 - Logo.width/2 , Screen.height/2 - 165,Logo.width,Logo.height),Logo);
		
		if(GUI.Button(new Rect(Screen.width/2 - 150,Screen.height/2 + buttonHeight*3,300,buttonHeight),"Campaign")){
			Application.LoadLevel("SelectSide");
		}
		if(GUI.Button(new Rect(Screen.width/2 - 150,Screen.height/2 + buttonHeight,300,buttonHeight),"Instant Action")){
			Application.LoadLevel("WWI_Ace_Rig");
		}

		if(GUI.Button(new Rect(Screen.width/2 - 150,Screen.height/2 + buttonHeight*2,300,buttonHeight),"Score Board")){
			Application.LoadLevel("LeaderBoardSampleScene");
		}

//		GUILayout.Box("", MenuSkin.GetStyle("panel_welcome"));
//		if (!FB.IsLoggedIn)                                                                                              
//		{                                                                                                                
//			GUI.Label((new Rect(179 , 11, 287, 160)), "Login to Facebook", MenuSkin.GetStyle("text_only"));             
//			if (GUI.Button(LoginButtonRect, "", MenuSkin.GetStyle("button_login")))                                      
//			{                                                                                                            
//				FB.Login("email,publish_actions", LoginCallback);                                                        
//			}                                                                                                            
//		}    


		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
	}

//	private void SetInit()                                                                       
//	{                                                                                            
//		Util.Log("SetInit");                                                                  
//		enabled = true; // "enabled" is a property inherited from MonoBehaviour                  
//		if (FB.IsLoggedIn)                                                                       
//		{                                                                                        
//			Util.Log("Already logged in");                                                    
//			OnLoggedIn();                                                                        
//		}                                                                                        
//	}                                                                                            
//	
//	private void OnHideUnity(bool isGameShown)                                                   
//	{                                                                                            
//		Util.Log("OnHideUnity");                                                              
//		if (!isGameShown)                                                                        
//		{                                                                                        
//			// pause the game - we will need to hide                                             
//			Time.timeScale = 0;                                                                  
//		}                                                                                        
//		else                                                                                     
//		{                                                                                        
//			// start the game back up - we're getting focus again                                
//			Time.timeScale = 1;                                                                  
//		}                                                                                        
//	}   
//
//	void LoginCallback(FBResult result)                                                        
//	{                                                                                          
//		Util.Log("LoginCallback");                                                          
//		
//		if (FB.IsLoggedIn)                                                                     
//		{                                                                                      
//			OnLoggedIn();                                                                      
//		}                                                                                      
//	}                                                                                          
//	
//	void OnLoggedIn()
//	{
//		Util.Log("Logged in. ID: " + FB.UserId);
//		
//		// Reqest player info and profile picture                                                                           
//		FB.API("/me?fields=id,first_name,friends.limit(100).fields(first_name,id)", Facebook.HttpMethod.GET, APICallback);  
//		LoadPicture(Util.GetPictureURL("me", 128, 128),MyPictureCallback);    
//	}   
//	void APICallback(FBResult result)                                                                                              
//	{                                                                                                                              
//		Util.Log("APICallback");                                                                                                
//		if (result.Error != null)                                                                                                  
//		{                                                                                                                          
//			Util.LogError(result.Error);                                                                                           
//			// Let's just try again                                                                                                
//			FB.API("/me?fields=id,first_name,friends.limit(100).fields(first_name,id)", Facebook.HttpMethod.GET, APICallback);     
//			return;                                                                                                                
//		}                                                                                                                          
//		
//		profile = Util.DeserializeJSONProfile(result.Text);                                                                        
//		GameStateManager.Username = profile["first_name"];                                                                         
//		friends = Util.DeserializeJSONFriends(result.Text);                                                                        
//	}                                                                                                                              
//	
//	void MyPictureCallback(Texture texture)                                                                                        
//	{                                                                                                                              
//		Util.Log("MyPictureCallback");                                                                                          
//		
//		if (texture == null)                                                                                                  
//		{                                                                                                                          
//			// Let's just try again
//			LoadPicture(Util.GetPictureURL("me", 128, 128),MyPictureCallback);                               
//			return;                                                                                                                
//		}                                                                                                                          
//		GameStateManager.UserTexture = texture;                                                                             
//	}      

}
