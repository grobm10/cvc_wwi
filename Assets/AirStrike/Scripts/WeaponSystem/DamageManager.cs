using UnityEngine;
using System.Collections;

public class DamageManager : MonoBehaviour
{
    public AudioClip[] HitSound;
    public GameObject Effect;
    public int HP = 100;
	public bool AI = true;

    private void Start()
    {
		if (AI == false)
			HP = PlayerPrefs.GetInt ("Health");
		//update with Playerprefs for Health
		else {
				}
    }

    public void ApplyDamage(int damage,GameObject killer)
    {
		if(HP<0)
		return;
	
        if (HitSound.Length > 0)
        {
            AudioSource.PlayClipAtPoint(HitSound[Random.Range(0, HitSound.Length)], transform.position);
        }
        HP -= damage;
        if (HP <= 0)
        {
			
			if(this.gameObject.GetComponent<FlightOnDead>()){
				this.gameObject.GetComponent<FlightOnDead>().OnDead(killer);
			}
            Dead();
        }
    }

    private void Dead()
    {
        if (Effect){
            GameObject obj = (GameObject)GameObject.Instantiate(Effect, transform.position, transform.rotation);
			if(this.rigidbody){
				if(obj.rigidbody){
					obj.rigidbody.velocity = this.rigidbody.velocity;
				}
			}
		}
		
		
        Destroy(this.gameObject);
    }

}
