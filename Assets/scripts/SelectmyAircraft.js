var aircraft : GameObject[];
var mySkin2 : GUISkin;
var index : int;

var screenWidth : int;
var screenHeight : int;
var	buttonHeight : int;

function Start(){

	screenWidth = Screen.width;
	screenHeight = Screen.height;
	var fButton : double  = Screen.height * 0.08;
	buttonHeight = fButton;
	index = PlayerPrefs.GetInt("myAircraft");
	
	if (index == null){
		index = 0;
	}
	aircraft[index].SetActiveRecursively(true);
}

function OnGUI(){
	
	var aircraftname = PlayerPrefs.GetString("Aircraft");
	var side = PlayerPrefs.GetString("Side");
	var mission = PlayerPrefs.GetString("MissionType");
	var speed = PlayerPrefs.GetFloat("Speed");
	PlayerPrefs.GetFloat("MaxSpeed");
	var health = PlayerPrefs.GetInt("Health");
	var RotationSpeed = PlayerPrefs.GetFloat("RotationSpeed");
	var TurnSpeed = PlayerPrefs.GetFloat("TurnSpeed");
	var PitchSpeed = PlayerPrefs.GetFloat("PitchSpeed");
	var RollSpeed = PlayerPrefs.GetFloat("RollSpeed");
	var YawSpeed = PlayerPrefs.GetFloat("YawSpeed");
	var climb = PlayerPrefs.GetFloat("DampingTarget");
	var Ammo = PlayerPrefs.GetFloat("Ammo");
	var Bomb = PlayerPrefs.GetFloat("Bomb");
	//PlayerPrefs.GetString("Weapons");
	var Fuel = PlayerPrefs.GetFloat("Fuel");
	
	GUI.skin = mySkin2;
	    GUI.BeginGroup(new Rect(10,buttonHeight,210,280));//,"Armiment");
	    GUI.Box(Rect(0,0,210,280),"Armiment");
	    GUI.Label(Rect(10,50,100,45),"MG Ammo: "+Ammo);
	    GUI.Label(Rect(10,100,100,45),"Bombs: "+Bomb);
	    GUI.Label(Rect(10,150,100,45),"Fuel: "+Fuel);
	    GUI.EndGroup();
	    
	    GUI.BeginGroup(new Rect(Screen.width-210,Screen.height-280,210,280));
	    GUI.Box(new Rect(0,buttonHeight-80,210,280),"");
	   	GUI.Label(Rect(10,10,100,40),"Name: "+aircraftname);
	    GUI.Label(Rect(10,50,100,45),"Turn Speed: "+TurnSpeed);
	    GUI.Label(Rect(10,100,100,45),"Speed: "+speed);
	    GUI.Label(Rect(10,150,100,45),"Climb: "+climb);
	    GUI.Label(Rect(10,200,100,45),"Manuvibility: "+((RotationSpeed+TurnSpeed+PitchSpeed+RollSpeed+YawSpeed)/5));
	    GUI.Label(Rect(10,220,100,45),"Durability: "+health);
	    
	    GUI.EndGroup();
	    
		if (GUI.Button (new Rect(Screen.width-(110),10,100,buttonHeight),"Next"))
		{
			index = index+1;
			if(index == aircraft.length){
				index = 0;
			}
			aircraft[0].SetActiveRecursively(false);
			aircraft[1].SetActiveRecursively(false);
			aircraft[2].SetActiveRecursively(false);
			aircraft[3].SetActiveRecursively(false);
			aircraft[4].SetActiveRecursively(false);
			aircraft[5].SetActiveRecursively(false);
			aircraft[6].SetActiveRecursively(false);
			aircraft[7].SetActiveRecursively(false);
			aircraft[8].SetActiveRecursively(false);
			aircraft[9].SetActiveRecursively(false);
			aircraft[10].SetActiveRecursively(false);
			aircraft[11].SetActiveRecursively(false);
			aircraft[12].SetActiveRecursively(false);
			aircraft[13].SetActiveRecursively(false);
////			aircraft[14].SetActiveRecursively(false);
////			aircraft[15].SetActiveRecursively(false);
			aircraft[index].SetActiveRecursively(true);

		}
		if (GUI.Button (new Rect(10,10,100,30),"Back"))
		{
			index = index-1;
			if (index == 0){
				index = aircraft.length;
			}
			aircraft[0].SetActiveRecursively(false);
			aircraft[1].SetActiveRecursively(false);
			aircraft[2].SetActiveRecursively(false);
			aircraft[3].SetActiveRecursively(false);
			aircraft[4].SetActiveRecursively(false);
			aircraft[5].SetActiveRecursively(false);
			aircraft[6].SetActiveRecursively(false);
			aircraft[7].SetActiveRecursively(false);
			aircraft[8].SetActiveRecursively(false);
			aircraft[9].SetActiveRecursively(false);
			aircraft[10].SetActiveRecursively(false);
			aircraft[11].SetActiveRecursively(false);
			aircraft[12].SetActiveRecursively(false);
			aircraft[13].SetActiveRecursively(false);
			aircraft[index].SetActiveRecursively(true);
		}
		if (GUI.Button (new Rect((Screen.width*.5)-(buttonHeight+5),10,100,buttonHeight),"TakeOff"))
		{
			PlayerPrefs.SetInt("myAircraft",index);
			Application.LoadLevel("WWI_Ace_Rig");
		}
		if (GUI.Button (new Rect(10,10,100,buttonHeight),"Back"))
		{
			PlayerPrefs.SetInt("myAircraft",index);
			Application.LoadLevel("MainMenu");
		}

}
