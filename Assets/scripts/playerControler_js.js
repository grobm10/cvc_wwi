﻿var noskin : GUISkin;
var turnSpeed : float = 3.0;
var maxTurnLean : float = 70.0;
var maxTilt : float = 50.0;
var propblurr : GameObject;
var sensitivity : float = 0.5;
//Force of the planes engine
var engineForce = 10000.0;

var drag = Vector3(2.0,8.0,0.05);
//stabilizing drag along x,y and z axes, causing plane to face forward
var stabilizingDrag = Vector3(2.0,1.0,0.0);

//Toruqe coefficient for elevator (pitch)
var elevator = 0.3;

//Base input for elevator (when no key is pressed), 
//so plane stays on one height without user input
var elevatorCenterSetting = -0.25;

//Toruqe coefficient for ailerons (roll)
var ailerons = 0.3;
var rudder = 0.3;
private var grounded = false;
private var dead = false;
private var rpmPitch = 0.0;
private var propOriginalRotation : Quaternion;
private var propSpin = 0.0;
//boost system (Torque based)
var boost = 0; //container value that should alway be zero
var boostrank = 0;// how strong the boost is
var boosttank = 0; //how long it lasts

if (boosttank == null){
	boosttank = 0.0;
}

var forwardForce : float = 5.0;
var guiSpeedElement : Transform;
var craft : GameObject;
//var tailv : Transform;
var flap1L : Transform;
var flap1R : Transform;
var Defaultaccelerator : Vector3;
var rotationSpeed : float;
var pitchSpeed : float;
var rudderSpeed : float;
//var flap2L : Transform;
//var flap2R : Transform;
var tailh : Transform;
var prop : Transform;
var gun : Transform;
var gun2 : Transform;
var flapangel : float = 1.0;
private var normalizedSpeed : float = 0.2;
private var euler : Vector3 = Vector3.zero;
var pitchval : float;
var rotationval : float;
var rudderval : float;
var horizontalOrientation : boolean = true;
var throttle : float;
var myControls : String = "flat";
var pitchdrift : float;
var velmass : float;
var brakeDrag = 0.0;
var brake = 10;
//ground brake and corner handling capabilities
var groundBrakeAccel = 8;
var groundCornerAccel = 6;
private var steerVelo : float;
private var wheel : Array;
private var rollInput = 0.0;


function ThrottleUp(){
	//normalizedSpeed = normalizedSpeed +0.03;
	//	if (normalizedSpeed <= 0) {
	//		normalizedSpeed = 0;
	//	}
	normalizedSpeed++;
}
function ThrottleDown(){
//	normalizedSpeed = normalizedSpeed -0.03;
//		if (normalizedSpeed <= 0) {
//			normalizedSpeed = 0;
//		}
	normalizedSpeed--;
}	

function firegun(){
	gun.SendMessage("Fire");
}

function IamDead(){
dead = true;
}

function Awake () {
	var turnspeed=PlayerPrefs.GetFloat("turnspeed");
	var maxlean=PlayerPrefs.GetFloat("maxlean");
	var maxtilt=PlayerPrefs.GetFloat("maxtilt");
	var forwardForce=PlayerPrefs.GetFloat("forwardForce");
	var rotationSpeed=PlayerPrefs.GetFloat("rotationSpeed");
	var pitchSpeed=PlayerPrefs.GetFloat("pitchSpeed");
	var rudderSpeed=PlayerPrefs.GetFloat("rudderSpeed");
	var flapangel=PlayerPrefs.GetFloat("flapangel");
	var Throttle= PlayerPrefs.GetFloat("Throttle");
	var Pitchdrift=PlayerPrefs.GetFloat("Pitchdrift");
	var GroundBrakeAccel= PlayerPrefs.GetFloat("GroundBrakeAccel");
	var GroundCornerAccel=PlayerPrefs.GetFloat("GroundCornerAccel");
	var CraftName= PlayerPrefs.GetString("CraftName");
	var Ammo=PlayerPrefs.GetInt("Ammo");
	var velmass =PlayerPrefs.GetFloat("velmass");
//	PlayerPrefs.GetInt("Bomb");
//	ScreenOrientation.AutoRotate= false;
	Screen.orientation = ScreenOrientation.LandscapeLeft;
	if (horizontalOrientation)
	{
			Screen.orientation = ScreenOrientation.LandscapeLeft;
	}
	else
	{
		Screen.orientation = ScreenOrientation.Portrait;
	}
	Defaultaccelerator = Input.acceleration;
//	DeviceMotionBinding.start();
	guiSpeedElement = GameObject.Find("speed").transform;
	guiSpeedElement.position = new Vector3 (0, normalizedSpeed, 0);
}

function FixedUpdate () {
//	GravityAndAcceleration gravAndAccel = DeviceMotionBinding.getGravityAndAcceleration();
	var myAircraft : GameObject;
//	var propMotion = Mathf.Clamp(Time.deltaTime*normalizedSpeed*360*80,0,43);
		
	if(GameObject.FindWithTag("SpaceCraft"))
	{
		myAircraft = GameObject.FindWithTag("SpaceCraft");
		myAircraft.rigidbody.AddRelativeForce(0, 0, normalizedSpeed * (forwardForce*3));
		var accelerator : Vector3 = Input.acceleration;
		var speedval = myAircraft.rigidbody.velocity.magnitude*0.1;
		var prop = GameObject.FindWithTag("Prop").transform;
		var tailh = GameObject.FindWithTag("tailh").transform;
		var tailv = GameObject.FindWithTag("tailv").transform;
		var flap1L = GameObject.FindWithTag("flap1L").transform;
		var flap1R = GameObject.FindWithTag("flap1R").transform;
		var gun = GameObject.FindWithTag("gun").transform;

		if (myControls== "flat"){
		if (Application.platform == RuntimePlatform.OSXEditor){
		//accelerator = Input.acceleration;
//		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
		
		accelerator.y = (Input.GetAxis("Horizontal")*-1);
		accelerator.x = (Input.GetAxis("Vertical")*30);
		accelerator.z = (Input.GetAxis("Rudder"));
		throttle = (Input.GetAxis("Jump"));
		normalizedSpeed = throttle;
		}
//		if (Application.platform == RuntimePlatform.OSXPlayer ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.WindowsEditor ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.OSXWebPlayer ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.WindowsWebPlayer){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
		if (horizontalOrientation)
		{
			var t : float = accelerator.x;
			accelerator.x = -accelerator.y;
			accelerator.y = t;
		}
		// Rotate turn based on acceleration		
		euler.y += accelerator.x * turnSpeed;
		// Since we set absolute lean position, do some extra smoothing on it
		euler.z = Mathf.Lerp(euler.z, -accelerator.x * maxTurnLean, 0.2);

		// Since we set absolute lean position, do some extra smoothing on it
		euler.x = Mathf.Lerp(euler.x, accelerator.y * maxTilt, 0.2);
	
		// Apply rotation and apply some smoothing
		var rot : Quaternion = Quaternion.Euler(euler);
		myAircraft.transform.rotation = Quaternion.Lerp (transform.rotation, rot, sensitivity);
//		if(prop)
//		{
//		propOriginalRotation=prop.localRotation;//		propSpin = Mathf.Repeat( propSpin+propMotion , 360);
//		prop.localRotation=Quaternion.Euler( 0,0,propMotion)*propOriginalRotation;	
//		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,normalizedSpeed));	
//		//prop.transform.Rotate (0.0, 0.0,(3000 * normalizedSpeed * Time.deltaTime));	
//		}
		audio.pitch= ((normalizedSpeed *speedval)*0.3);
		audio.volume= 0.2;
		tailv.transform.localRotation.y = ((rudderval-1.5)* 0.3*-1)*flapangel;
		flap1L.transform.localRotation.x = (accelerator.x* 0.3)*flapangel;
		flap1R.transform.localRotation.x = (accelerator.x*-0.3)*flapangel;
		tailh.transform.localRotation.x =(accelerator.y*-0.3)*flapangel;
		}
		
		if (myControls== "pro"){
		if (Application.platform == RuntimePlatform.OSXEditor){
		//accelerator : Vector3 = Input.acceleration;
//		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
		
		accelerator.y = (Input.GetAxis("Horizontal")*-1);
		accelerator.x = (Input.GetAxis("Vertical")*30);
		accelerator.z = (Input.GetAxis("Rudder"));
		throttle = (Input.GetAxis("Jump"));
		normalizedSpeed = throttle;
		
		}
//		if (Application.platform == RuntimePlatform.OSXPlayer ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.WindowsEditor ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.OSXWebPlayer ){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		if (Application.platform == RuntimePlatform.WindowsWebPlayer){
//		//accelerator : Vector3 = Input.acceleration;
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.y = (Input.GetAxis("Horizontal")*-1);
//		accelerator.x = (Input.GetAxis("Vertical"));
//		accelerator.z = (Input.GetAxis("Rudder"));
//		throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
		pitchval = (accelerator.z - Defaultaccelerator.z) * (pitchSpeed)*(sensitivity*.2)*-1;
    	rotationval = ( accelerator.y - Defaultaccelerator.y) *(rotationSpeed)*(sensitivity*.2);
//    	rudderval = ( accelerator.x - Defaultaccelerator.x) *(rudderSpeed)*(sensitivity)*-1;
//    	rudderval *=Time.deltaTime;
//    	pitchval *=Time.deltaTime;
 		myAircraft.rigidbody.AddRelativeForce(0, 0, (normalizedSpeed * forwardForce*-1));//*-1   //-1
		myAircraft.transform.Rotate (pitchval, (rudderval-1.5), rotationval);
		audio.pitch= ((normalizedSpeed *speedval)*0.3);
		audio.volume= 0.2;
		tailv.transform.localRotation.y = ((rudderval-1.5)* 0.3)*-1*flapangel;
		flap1L.transform.localRotation.x = (accelerator.y*0.3)*flapangel;
		flap1R.transform.localRotation.x = (accelerator.y*-0.3)*flapangel;
		tailh.transform.localRotation.x =(accelerator.z*0.3)*flapangel;
		
		}
		
		if(prop)
		{
		propOriginalRotation=prop.localRotation;
		var propMotion = Mathf.Clamp(Time.deltaTime*normalizedSpeed*360*80,0,43);
		prop.localRotation=Quaternion.Euler( 0,0,propMotion)*propOriginalRotation;	
		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,normalizedSpeed));	
		}
//		audio.pitch= ((normalizedSpeed *speedval)*0.3);
//		audio.volume= 0.2;
//		flap1L.transform.localRotation.x = (accelerator.x* 0.3)*flapangel;
//		flap1R.transform.localRotation.x = (accelerator.x*-0.3)*flapangel;
//		tailh.transform.localRotation.x =(accelerator.y*-0.3)*flapangel;
	}
}

var btnTexture : Texture2D;
var btnTexture2 : Texture2D;
var btnTexture3: Texture2D;


//function ThrottleUp(){
//	normalizedSpeed++;	
//}
//
//function ThrottleDown(){
//	normalizedSpeed--;
//}

function OnGUI(){
	  GUI.skin = noskin;
	var throttleper : float = normalizedSpeed*100;
	if (GUI.RepeatButton (Rect (10,Screen.height*.5,64,64), "+" )) {
		normalizedSpeed = normalizedSpeed +0.03;
		if (normalizedSpeed >= 1) {
			normalizedSpeed = 1;
		}
//		
////		print(normalizedSpeed);
	}
	
	if (GUI.RepeatButton (Rect (10,Screen.height-75,64,64), "-")) {
		normalizedSpeed = normalizedSpeed -0.03;
		if (normalizedSpeed <= 0) {
			normalizedSpeed = 0;
		}
//	
//	
	}
	if (myControls== "pro"){
	   if (GUI.Button(Rect(Screen.width*.5-25,Screen.height-25,50,32),"")){
	   rudderval = 1.5;
	   }
	   rudderval = GUI.HorizontalScrollbar (Rect (Screen.width*.5-50, Screen.height-60, 100, 30), rudderval, 1.0, 0.0, 4.0);
	}
	if (GUI.RepeatButton(Rect(10,Screen.height*.5,64,64),btnTexture))
		normalizedSpeed++;	
	
	if (GUI.RepeatButton(Rect(10,Screen.height-75,64,64),btnTexture2))
		normalizedSpeed--;
	if (GUI.RepeatButton(Rect(Screen.width-70,Screen.height-70,64,64),"fire"))
		if(gun){
			gun.SendMessage("Fire");
		}
		if(gun2){
			gun.SendMessage("Fire");
		}			
}



function GroundPhysics() {
	var velo=rigidbody.velocity;

	var dir=transform.TransformDirection(Vector3.forward);
	var flatDir=Vector3.Normalize(new Vector3(dir.x,0,dir.z));
	var flatVelo=new Vector3(velo.x,0,velo.z);
	var brakeForce=-flatVelo.normalized*brake*rigidbody.mass*groundBrakeAccel;

	flatDir*=flatVelo.magnitude;
	var rev=Vector3.Dot(flatVelo,flatDir)>0?1:-1;
	flatDir*=rev;
	var diff=(flatVelo-flatDir).magnitude;
	var cornerAccel=groundCornerAccel;
	if(cornerAccel>diff)cornerAccel=diff;
	var cornerForce=-(flatVelo-flatDir).normalized*cornerAccel*rigidbody.mass;
	var cornerSlip=Mathf.Pow(cornerAccel/groundCornerAccel,3);
	
	rigidbody.AddForce(brakeForce+cornerForce);
	
	var fVelo=velo.magnitude;
	var veloSteer=((15/(2*fVelo+1))+1);
	var maxRotSteer=0.5*Time.fixedDeltaTime*(1-0.8*cornerSlip);
	var veloFactor=fVelo<1.0?fVelo:Mathf.Pow(fVelo,0.1);
	var steerVeloInput=rev*rollInput*veloFactor*0.5*Time.fixedDeltaTime;
	if(fVelo<0.1)
		steerVeloInput=0;
	if(steerVeloInput>steerVelo)
	{
		steerVelo+=0.02*Time.fixedDeltaTime*veloSteer;
		if(steerVeloInput<steerVelo)
			steerVelo=steerVeloInput;
	}
	else
	{
		steerVelo-=0.02*Time.fixedDeltaTime*veloSteer;
		if(steerVeloInput>steerVelo)
			steerVelo=steerVeloInput;
	}
	steerVelo=Mathf.Clamp(steerVelo,-maxRotSteer,maxRotSteer);	
	transform.Rotate(Vector3.up*steerVelo*57.295788);	
}

//physics for moving the plane through the air
function AirPhysics () {

	//engine force
	rigidbody.AddForce(transform.forward*engineForce*throttle*boost);
	
	//wings and drag
	var forwardVelo = Vector3.Dot(rigidbody.velocity,transform.forward);
	var sqrVelo = forwardVelo*forwardVelo;

	var dragDirection = transform.InverseTransformDirection(rigidbody.velocity);
	var dragAndBrake = drag+Vector3(0,0,brakeDrag*brake);
	var dragForces = -Vector3.Scale(dragDirection,dragAndBrake)*rigidbody.velocity.magnitude;
	rigidbody.AddForce(transform.TransformDirection(dragForces));

	//stabilization (to keep the plane facing into the direction it's moving)
	var stabilizationForces = -Vector3.Scale(dragDirection,stabilizingDrag)*rigidbody.velocity.magnitude;
	
	rigidbody.AddForceAtPosition(transform.TransformDirection(stabilizationForces),transform.position-transform.forward*10);
	rigidbody.AddForceAtPosition(-transform.TransformDirection(stabilizationForces),transform.position+transform.forward*10);
		
	//elevator
	rigidbody.AddTorque(transform.right*sqrVelo*elevator*(pitchval+elevatorCenterSetting));	

	//ailerons
	if(!grounded)
		rigidbody.AddTorque(-transform.forward*sqrVelo*ailerons*rollInput);	
	//rudder
	rigidbody.AddTorque(transform.up*sqrVelo*rudder*rudderval);	

	//sound
	rpmPitch=Mathf.Lerp(rpmPitch,throttle,Time.deltaTime*0.2);
	audio.pitch=1.0+1.0*rpmPitch;
	audio.volume=0.4+throttle;
	
	//prop
	if(prop != null)
	{
		var propMotion=Mathf.Clamp(Time.deltaTime*rpmPitch*360*80,0,43);
		var propSpin = Mathf.Repeat( propSpin+propMotion , 360);
		prop.localRotation=Quaternion.Euler( 0,0,propSpin)*propOriginalRotation;	
		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,rpmPitch));	
	}
}
//var turnSpeed : float = 3.0;
//var maxTurnLean : float = 70.0;
//var maxTilt : float = 50.0;
//
//var sensitivity : float = 0.5;
////Force of the planes engine
//var engineForce = 10000.0;
//
//var drag = Vector3(2.0,8.0,0.05);
////stabilizing drag along x,y and z axes, causing plane to face forward
//var stabilizingDrag = Vector3(2.0,1.0,0.0);
//
////Toruqe coefficient for elevator (pitch)
//var elevator = 0.3;
//
////Base input for elevator (when no key is pressed), 
////so plane stays on one height without user input
//var elevatorCenterSetting = -0.25;
//var defaultTrim = 0.04;
//
////Toruqe coefficient for ailerons (roll)
//var ailerons = 0.3;
//var rudder = 0.3;
//private var grounded = false;
//private var dead = false;
//private var rpmPitch = 0.0;
//private var propOriginalRotation : Quaternion;
//private var propSpin = 0.0;
////boost system (Torque based)
//var boost = 0; //container value that should alway be zero
//var boostrank = 0;// how strong the boost is
//var boosttank = 0; //how long it lasts
//
//if (boosttank == null){
//	boosttank = 0.0;
//}
//
//var forwardForce : float = 5.0;
//var guiSpeedElement : Transform;
//var craft : GameObject;
////var tailv : Transform;
//var flap1L : Transform;
//var flap1R : Transform;
//var Defaultaccelerator : Vector3;
//var rotationSpeed : float;
//var pitchSpeed : float;
//var rudderSpeed : float;
////var flap2L : Transform;
////var flap2R : Transform;
//var tailh : Transform;
//var prop : Transform;
//var gun : Transform;
////var gun2 : Transform;
//var flapangel : float = 1.0;
//private var normalizedSpeed : float = 0.2;
//private var euler : Vector3 = Vector3.zero;
//var pitchval : float;
//var rotationval : float;
//var rudderval : float;
//var horizontalOrientation : boolean = true;
//var throttle : float;
//var myControls : String = "flat";
//var pitchdrift : float;
//var velmass : float;
//var brakeDrag = 0.0;
//var brake = 10;
////ground brake and corner handling capabilities
//var groundBrakeAccel = 8;
//var groundCornerAccel = 6;
//var myAircraft : GameObject;
//var autotrim : String;
//var nolimits : String;
//
//private var steerVelo : float;
//private var wheel : Array;
//private var rollInput = 0.0;
//
//function Awake () {
//	var turnspeed=PlayerPrefs.GetFloat("turnspeed");
//	var maxlean=PlayerPrefs.GetFloat("maxlean");
//	var maxtilt=PlayerPrefs.GetFloat("maxtilt");
//	var forwardForce=PlayerPrefs.GetFloat("forwardForce");
//	var rotationSpeed=PlayerPrefs.GetFloat("rotationSpeed");
//	var pitchSpeed=PlayerPrefs.GetFloat("pitchSpeed");
//	var rudderSpeed=PlayerPrefs.GetFloat("rudderSpeed");
//	var flapangel=PlayerPrefs.GetFloat("flapangel");
//	var Throttle= PlayerPrefs.GetFloat("Throttle");
//	var Pitchdrift=PlayerPrefs.GetFloat("Pitchdrift");
//	var GroundBrakeAccel= PlayerPrefs.GetFloat("GroundBrakeAccel");
//	var GroundCornerAccel=PlayerPrefs.GetFloat("GroundCornerAccel");
//	var CraftName= PlayerPrefs.GetString("CraftName");
//	var Ammo= PlayerPrefs.GetInt("Ammo"); //30000;//
//	var velmass =PlayerPrefs.GetFloat("velmass");
//	var myControls=PlayerPrefs.GetString("myControls");
//	var autotrim=PlayerPrefs.GetString("autotrim");
//	var nolimits=PlayerPrefs.GetString("nolimits");
//	
//	if (Application.platform == RuntimePlatform.IPhonePlayer){
////	PlayerPrefs.GetInt("Bomb");
//	iPhoneKeyboard.autorotateToPortrait = false;
//	iPhoneKeyboard.autorotateToPortraitUpsideDown = false;
//	iPhoneKeyboard.autorotateToLandscapeRight = false;
//	iPhoneKeyboard.autorotateToLandscapeLeft = false;
//	if (horizontalOrientation)
//	{
//	  iPhoneSettings.screenOrientation =
//			iPhoneScreenOrientation.LandscapeLeft;
//	}
//	else
//	{
//		Screen.orientation =
//			ScreenOrientation.Portrait;
//	}
//	Defaultaccelerator = Input.acceleration;
//	}
//	
////	DeviceMotionBinding.start();
////	guiSpeedElement = GameObject.Find("speed").transform;
////	guiSpeedElement.position = new Vector3 (0, normalizedSpeed, 0);
//}
//
//function FixedUpdate () {
////	GravityAndAcceleration gravAndAccel = DeviceMotionBinding.getGravityAndAcceleration();
////	var propMotion = Mathf.Clamp(Time.deltaTime*normalizedSpeed*360*80,0,43);
//	if (!dead){	
//	if(GameObject.FindWithTag("SpaceCraft"))
//	{
//		myAircraft = GameObject.FindWithTag("SpaceCraft");
//		myAircraft.rigidbody.AddRelativeForce(0, 0, normalizedSpeed * (forwardForce*3));
//		var accelerator : Vector3 = Input.acceleration;
//		var speedval = myAircraft.rigidbody.velocity.magnitude*0.1;
//		var prop = GameObject.FindWithTag("Prop").transform;
//		var tailh = GameObject.FindWithTag("tailh").transform;
//		var tailv = GameObject.FindWithTag("tailv").transform;
//		var flap1L = GameObject.FindWithTag("flap1L").transform;
//		var flap1R = GameObject.FindWithTag("flap1R").transform;
//		// var fuselog = GameObject.Find("/Hand");
////		gun2 = GameObject.FindWithTag("gun2").transform;
//
//		if (myControls== "flat"){
//		if (Application.platform != RuntimePlatform.IPhonePlayer ){
////		accelerator.
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
//		
//		accelerator.x = Input.acceleration.x;//(Input.GetAxis("Horizontal"));
//		accelerator.y = Input.acceleration.z;//(Input.GetAxis("Vertical")*-1);
//		accelerator.z = (Input.GetAxis("Rudder"));
//		// throttle = (Input.GetAxis("Jump"));
//		normalizedSpeed = throttle;
//		}
//		 if (horizontalOrientation)
//		{
//			var t : float = accelerator.x;
//			accelerator.x = -accelerator.y;
//			accelerator.y = t;
//		}
//		// Rotate turn based on acceleration		
//		euler.y += accelerator.x * turnSpeed;
//		// Since we set absolute lean position, do some extra smoothing on it
//		euler.z = Mathf.Lerp(euler.z, -accelerator.x * maxTurnLean, 0.2);
//
//		// Since we set absolute lean position, do some extra smoothing on it
//		euler.x = Mathf.Lerp(euler.x, accelerator.y * maxTilt, 0.2);
//	
//		// Apply rotation and apply some smoothing
//		var rot : Quaternion = Quaternion.Euler(euler);
//		myAircraft.transform.rotation = Quaternion.Lerp (transform.rotation, rot, sensitivity);
////		if(prop)
////		{
////		propOriginalRotation=prop.localRotation;//		propSpin = Mathf.Repeat( propSpin+propMotion , 360);
////		prop.localRotation=Quaternion.Euler( 0,0,propMotion)*propOriginalRotation;	
////		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,normalizedSpeed));	
////		//prop.transform.Rotate (0.0, 0.0,(3000 * normalizedSpeed * Time.deltaTime));	
//		}
//		audio.pitch= ((normalizedSpeed *speedval)*0.3);
//		audio.volume= 0.2;
//		tailv.transform.localRotation.y = ((rudderval-1.5)* 0.3*-1)*flapangel;
//		flap1L.transform.localRotation.x = (accelerator.x* 0.3)*flapangel;
//		flap1R.transform.localRotation.x = (accelerator.x*-0.3)*flapangel;
//		tailh.transform.localRotation.x =(accelerator.y*-0.3)*flapangel;
//		}
//		
//		if (myControls== "pro"){
//			if (Application.platform != RuntimePlatform.IPhonePlayer ){
////		accelerator.
////		var gyroQuaternion : Quaternion  = DeviceMotionBinding.getNormalizedQuarternion();
////		accelerator = Input.acceleration;
//		
//		accelerator.y = Input.acceleration.x;//(Input.GetAxis("Horizontal")*-1);
//		accelerator.z = Input.acceleration.z;//(Input.GetAxis("Vertical"));
//		// accelerator.z = (Input.GetAxis("Rudder"));
////		throttle = (Input.GetAxis("Jump"));
//		if (autotrim =="on"){
//			elevatorCenterSetting = 0.000;
//		}
//		if (autotrim !="on"){
//			elevatorCenterSetting = defaultTrim;
//		}
//		
//		var trim = (elevatorCenterSetting*speedval)*.5;
//		accelerator.z = accelerator.z + trim; //elevatorCenterSetting;
////		normalizedSpeed = throttle;
//		// }
//		pitchval = (accelerator.z - Defaultaccelerator.z) * (pitchSpeed)*(sensitivity*.02)*-1;
//    	rotationval = ( accelerator.y - Defaultaccelerator.y) *(rotationSpeed)*(sensitivity*.02);
////    	rudderval = ( accelerator.x - Defaultaccelerator.x) *(rudderSpeed)*(sensitivity)*-1;
////    	rudderval *=Time.deltaTime;
////    	pitchval *=Time.deltaTime;
//	if (myAircraft != null){
// 		myAircraft.rigidbody.AddRelativeForce(0, 0, (normalizedSpeed * forwardForce*-1));//*-1   //-1
//		myAircraft.transform.Rotate (pitchval*speedval, (rudderval-1.5), rotationval*speedval);
//		
//		audio.pitch= ((normalizedSpeed *speedval)*0.3);
//		audio.volume= 0.2;
//		tailv.transform.localRotation.y = ((rudderval-1.5)* 0.3)*-1*flapangel;
//		flap1L.transform.localRotation.x = (accelerator.y*0.3)*flapangel;
//		flap1R.transform.localRotation.x = (accelerator.y*-0.3)*flapangel;
//		tailh.transform.localRotation.x =(accelerator.z*0.3)*flapangel;
//		
//		}
//		
//		if(prop)
//		{
//		propOriginalRotation=prop.localRotation;
//		var propMotion = Mathf.Clamp(Time.deltaTime*normalizedSpeed*360*80,0,43);
//		prop.localRotation=Quaternion.Euler( 0,0,propMotion)*propOriginalRotation;	
//		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,normalizedSpeed));	
//		}
//		}
////		audio.pitch= ((normalizedSpeed *speedval)*0.3);
////		audio.volume= 0.2;
////		flap1L.transform.localRotation.x = (accelerator.x* 0.3)*flapangel;
////		flap1R.transform.localRotation.x = (accelerator.x*-0.3)*flapangel;
////		tailh.transform.localRotation.x =(accelerator.y*-0.3)*flapangel;
//	}
//	}
//	
//}
//
////function Update () {
//////	if (Application.platform == RuntimePlatform.OSXEditor){
//////		normalizedSpeed = Mathf.Clamp01(Input.GetAxis("Jump"));
//////		guiSpeedElement.position = new Vector3 (0, normalizedSpeed, 0);
//////	}
////	
////	for (var evt : iPhoneTouch in iPhoneInput.touches)
////	{
////		if (evt.phase == iPhoneTouchPhase.Moved)
////		{
////			normalizedSpeed = evt.position.y / Screen.height;
////			guiSpeedElement.position = new Vector3 (0, normalizedSpeed, 0);
////		}
////	}
////}
////var btnTexture : Texture2D;
////var btnTexture2 : Texture2D;
////var btnTexture3: Texture2D;
//var mySkin : GUISkin;
//var clearSkin : GUISkin;
//
//function OnGUI(){
//	GUI.skin = clearSkin;
//	var throttleper : float = normalizedSpeed*100;
//	if (GUI.RepeatButton(Rect(Screen.width-70,Screen.height-70,64,64),"")){
//		if(gun == GameObject.FindWithTag("gun").transform){
//			firegun();
//		}
//		else
//			{
//			}
////		if(gun2){
////			gun2.SendMessage("Fire");
////		}
//	}
//	if (GUI.RepeatButton (Rect (10,Screen.height*.5,64,64), "" )) {
//		normalizedSpeed = normalizedSpeed +0.03;
//		if (normalizedSpeed >= 1) {
//			normalizedSpeed = 1;
//		}
//		
////		print(normalizedSpeed);
//	}
//	if (GUI.RepeatButton (Rect (10,Screen.height-75,64,64), "")) {
//		normalizedSpeed = normalizedSpeed -0.03;
//		if (normalizedSpeed <= 0) {
//			normalizedSpeed = 0;
//		}
//	
//	
//	}
//	
//	if (myControls== "pro"){
//		GUI.skin = mySkin;
//	
//	   if (GUI.Button(Rect(Screen.width*.5-25,Screen.height-25,50,32),"")){
//	   rudderval = 1.5;
//	   }
//	   
//	   rudderval = GUI.HorizontalScrollbar (Rect (Screen.width*.5-50, Screen.height-60, 100, 30), rudderval, 1.0, 0.0, 4.0);
//	}
//
//	
////	if (GUI.RepeatButton(Rect(10,Screen.height*.5,64,64),btnTexture))
////		normalizedSpeed++;	
////	
////	if (GUI.RepeatButton(Rect(10,Screen.height-75,64,64),btnTexture2))
////		normalizedSpeed--;
//				
//}
//
//function GroundPhysics() {
//	var velo=rigidbody.velocity;
//
//	var dir=transform.TransformDirection(Vector3.forward);
//	var flatDir=Vector3.Normalize(new Vector3(dir.x,0,dir.z));
//	var flatVelo=new Vector3(velo.x,0,velo.z);
//	var brakeForce=-flatVelo.normalized*brake*rigidbody.mass*groundBrakeAccel;
//
//	flatDir*=flatVelo.magnitude;
//	var rev=Vector3.Dot(flatVelo,flatDir)>0?1:-1;
//	flatDir*=rev;
//	var diff=(flatVelo-flatDir).magnitude;
//	var cornerAccel=groundCornerAccel;
//	if(cornerAccel>diff)cornerAccel=diff;
//	var cornerForce=-(flatVelo-flatDir).normalized*cornerAccel*rigidbody.mass;
//	var cornerSlip=Mathf.Pow(cornerAccel/groundCornerAccel,3);
//	
//	rigidbody.AddForce(brakeForce+cornerForce);
//	
//	var fVelo=velo.magnitude;
//	var veloSteer=((15/(2*fVelo+1))+1);
//	var maxRotSteer=0.5*Time.fixedDeltaTime*(1-0.8*cornerSlip);
//	var veloFactor=fVelo<1.0?fVelo:Mathf.Pow(fVelo,0.1);
//	var steerVeloInput=rev*rollInput*veloFactor*0.5*Time.fixedDeltaTime;
//	if(fVelo<0.1)
//		steerVeloInput=0;
//	if(steerVeloInput>steerVelo)
//	{
//		steerVelo+=0.02*Time.fixedDeltaTime*veloSteer;
//		if(steerVeloInput<steerVelo)
//			steerVelo=steerVeloInput;
//	}
//	else
//	{
//		steerVelo-=0.02*Time.fixedDeltaTime*veloSteer;
//		if(steerVeloInput>steerVelo)
//			steerVelo=steerVeloInput;
//	}
//	steerVelo=Mathf.Clamp(steerVelo,-maxRotSteer,maxRotSteer);	
//	transform.Rotate(Vector3.up*steerVelo*57.295788);	
//}
//
////physics for moving the plane through the air
//function AirPhysics () {
//
//	//engine force
//	rigidbody.AddForce(transform.forward*engineForce*throttle*boost);
//	
//	//wings and drag
//	var forwardVelo = Vector3.Dot(rigidbody.velocity,transform.forward);
//	var sqrVelo = forwardVelo*forwardVelo;
//
//	var dragDirection = transform.InverseTransformDirection(rigidbody.velocity);
//	var dragAndBrake = drag+Vector3(0,0,brakeDrag*brake);
//	var dragForces = -Vector3.Scale(dragDirection,dragAndBrake)*rigidbody.velocity.magnitude;
//	rigidbody.AddForce(transform.TransformDirection(dragForces));
//
//	//stabilization (to keep the plane facing into the direction it's moving)
//	var stabilizationForces = -Vector3.Scale(dragDirection,stabilizingDrag)*rigidbody.velocity.magnitude;
//	
//	rigidbody.AddForceAtPosition(transform.TransformDirection(stabilizationForces),transform.position-transform.forward*10);
//	rigidbody.AddForceAtPosition(-transform.TransformDirection(stabilizationForces),transform.position+transform.forward*10);
//		
//	//elevator
//	rigidbody.AddTorque(transform.right*sqrVelo*elevator*(pitchval+elevatorCenterSetting));	
//
//	//ailerons
//	if(!grounded)
//		rigidbody.AddTorque(-transform.forward*sqrVelo*ailerons*rollInput);	
//	//rudder
//	rigidbody.AddTorque(transform.up*sqrVelo*rudder*rudderval);	
//
//	//sound
//	rpmPitch=Mathf.Lerp(rpmPitch,throttle,Time.deltaTime*0.2);
//	audio.pitch=1.0+1.0*rpmPitch;
//	audio.volume=0.4+throttle;
//	
//	//prop
//	if(prop != null)
//	{
//		var propMotion=Mathf.Clamp(Time.deltaTime*rpmPitch*360*80,0,43);
//		var propSpin = Mathf.Repeat( propSpin+propMotion , 360);
//		prop.localRotation=Quaternion.Euler( 0,0,propSpin)*propOriginalRotation;	
//		prop.renderer.material.SetColor("_BlurColor",Color(1,1,1,rpmPitch));	
//	}
//}
