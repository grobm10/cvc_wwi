﻿// This example assumes that the user has entered his name into a
// variable called name and that score contains the user's current
// score.
var highscore_url = "http://www.grobgames/highscores.pl";
var playName = "Player 1";
var score = -1;
var myhighscore_txt = "none";

function Start() {
	// Create a form object for sending high score data to the server
	var form = new WWWForm();
	// Assuming the perl script manages high scores for different games
	form.AddField( "game", "MyGameName" );
	 // The name of the player submitting the scores
	form.AddField( "playerName", playName );
	 // The score
	form.AddField( "score", score );

	// Create a download object
	var download = new WWW( highscore_url, form );

	// Wait until the download is done
	yield download;

	if(download.error) {
		print( "Error downloading: " + download.error );
		return;
	} else {
		// show the highscores
		myhighscore_txt = download.text;
		Debug.Log(download.text);
	}
}

function OnGUI(){
   GUI.Label(new Rect(10, 150, 100, 20), "text: "+myhighscore_txt);
   }