﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	public int counter = 0;     
	public AsyncOperation loadOp;
	
	void Start()    
	{   
		loadOp = Application.LoadLevelAsync("MainMenu");    
	}
	
	void OnGUI()    
	{    
		string loadString = "Loading "+loadOp.progress.ToString()+"%";    
		//TEMP: trying to see if loadlevelaync works or not...    
		counter++;    
		loadString += " (" + counter.ToString() + ")";    
		GUILayout.Label(loadString);
	}    

}

//public class MainMenu: MonoBehaviour     
//{    
//	public int counter = 0;     
//	public AsyncOperation loadOp;
//	
//	void Start()    
//	{   
//		loadOp = Application.LoadLevelAsync("freewayLevel");    
//	}
//	
//	void OnGUI()    
//	{    
//		string loadString = "Loading "+loadOp.progress.ToString()+"%";    
//		//TEMP: trying to see if loadlevelaync works or not...    
//		counter++;    
//		loadString += " (" + counter.ToString() + ")";    
//		GUILayout.Label(loadString);    
//	}    
//}
