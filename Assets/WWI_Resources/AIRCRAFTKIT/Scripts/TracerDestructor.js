// Aircraft Kit �2010 Laurentiu Danila/Apparatus

var explosion : GameObject;
var splash : GameObject;
var shipExplosion : GameObject;
var rockExplosion : GameObject;
var Splash: AudioClip;
var Bang: AudioClip;

function Update () {
Destroy (gameObject, 1);
}

function OnCollisionEnter(collision : Collision) {
	var contact : ContactPoint = collision.contacts[0];
	
   	if (collision.gameObject.tag=="Player")
	{
		audio.clip = Bang;
		audio.Play();
		Destroy (gameObject);
	}
	
	if (collision.gameObject.tag=="Water")
	{
		audio.clip = Splash;
		audio.Play();
		Destroy (gameObject);
	}
	
}
