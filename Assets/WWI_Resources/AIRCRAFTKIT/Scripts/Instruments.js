// Aircraft Kit �2010 Laurentiu Danila/Apparatus

// Airspeed

var dialTex: Texture2D;
var needleTex: Texture2D;
var dialPos: Vector2;
var topSpeed: float = 200.00;
var stopAngle: float = 160;
var topSpeedAngle: float = 400;
var speed: float = 250;

// Altimeter

var altDialTex: Texture2D;
var altNeedleTex: Texture2D;
var altDialPos: Vector2;
var topAltitude: float = 500;
var altStopAngle: float = 160;
var topAltitudeAngle: float = 400;
var altitude: float = 250;
var hit : RaycastHit; 

function Start(){
dialPos.x = (Screen.width*.5)+100;
dialPos.y = Screen.height-70;	

altDialPos.x = (Screen.width*.5)-164;
altDialPos.y = Screen.height-70;
}
function Update (){
	if(GameObject.FindWithTag("SpaceCraft"))
	{
		var myAircraft = GameObject.FindWithTag("SpaceCraft");

	speed = myAircraft.rigidbody.velocity.magnitude * 2.237;
	altitude = myAircraft.rigidbody.transform.position.y;
	}
}

function OnGUI () {
	
	GUI.DrawTexture(Rect(dialPos.x, dialPos.y, dialTex.width, dialTex.height), dialTex);
	var centre = Vector2(dialPos.x + dialTex.width / 2, dialPos.y + dialTex.height / 2);
	var savedMatrix = GUI.matrix;
	var speedFraction = speed / topSpeed;
	var needleAngle = Mathf.Lerp(stopAngle, topSpeedAngle, speedFraction);
	GUIUtility.RotateAroundPivot(needleAngle, centre);
	GUI.DrawTexture(Rect(centre.x, centre.y - needleTex.height / 2, needleTex.width, needleTex.height), needleTex);
	GUI.matrix = savedMatrix;
	
	GUI.DrawTexture(Rect(altDialPos.x, altDialPos.y, altDialTex.width, altDialTex.height), altDialTex);
	var altCentre = Vector2(altDialPos.x + altDialTex.width / 2, altDialPos.y + altDialTex.height / 2);
	var altSavedMatrix = GUI.matrix;
	var altitudeFraction = altitude / topAltitude;
	var altNeedleAngle = Mathf.Lerp(stopAngle, topAltitudeAngle, altitudeFraction);
	GUIUtility.RotateAroundPivot(altNeedleAngle, altCentre);
	GUI.DrawTexture(Rect(altCentre.x, altCentre.y - altNeedleTex.height / 2, altNeedleTex.width, altNeedleTex.height), altNeedleTex);
	GUI.matrix = altSavedMatrix;
	
	GUI.Label (Rect (Screen.width-200, 20, 150, 20), "Ammo: "+PlayerPrefs.GetInt("Ammo"));
	GUI.Label (Rect (Screen.width-200, 50, 100, 20), "Bombs: "+PlayerPrefs.GetInt("Bombs"));
}
