﻿using UnityEngine;
using System.Collections;

public class MissionArea : MonoBehaviour {

	public double Timer = 60;
	private bool area = false;
	private int screenWidth;
	private int screenHeight;
	public int buttonHeight;
	
	void Start () {
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		double fButton = Screen.height * 0.08;
		buttonHeight = (int)fButton;
	}

	void OnTriggerEnter(Collider other) {
		area = true;
		
	}
	void OnTriggerStay(Collider other) {
		area = true;
		
	}
	void OnTriggerExit(Collider other) {
		area = false;
		Timer = 60;
		
	}

	public void OnGUI ()
	{
		if (area == true) {
			Timer = Timer - 0.1;

			if (Timer <= 0.00){
				GameManager gamemanger = (GameManager)GameObject.FindObjectOfType (typeof(GameManager));
				gamemanger.GameOver ();
			}

			GUI.Label (new Rect (20, 90, 200, buttonHeight), "Timer: " + Timer.ToString ());
				}

		else if (area == false)
			return;
	}
}