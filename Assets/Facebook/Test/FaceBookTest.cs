using UnityEngine;
using LitJson;
using System.Collections;

public class FaceBookTest : MonoBehaviour {
	
	string lastResponse = "last response: none";
	public GameObject portrait;
	
	// Use this for initialization
	void Start () {
		Facebook.onLogin += delegate( bool _success, JsonData _response ) 
		{
			if( _success ) {
				StartCoroutine( getImage ( Facebook.getProfilePictureURL( Facebook.user_me ) ) );
				lastResponse = "Login OK";
				_response.ToString();
			}
			else
				lastResponse = "Login Fail";
		};
		
		Facebook.onGraphRequest += delegate( bool _success, JsonData _response ) 
		{
			if( _success )
				lastResponse =  _response.ToJson() ;
			else
				lastResponse = "Request Fail";
		};
	}
	
	#region TEST	
	public IEnumerator getImage( string _imageURL )
	{
        var www = new WWW(_imageURL); 
        yield return www; 
        portrait.renderer.material.mainTexture = www.texture;
    }
	
	void OnGUI()
	{
		GUI.Label( new Rect (120, 10, 400, 300) , lastResponse );
		
		if( GUI.Button( new Rect( 10, 10, 100, 30), "Login/Authorize" ) )
		{
			Facebook.login( "email,publish_actions,publish_stream" );
		}
		if( GUI.Button( new Rect( 10, 40, 100, 30), "AppRequest" ) )
		{
			Facebook.uiAppRequest( "MyRequest", "please use my app" );
		}
		if( GUI.Button( new Rect( 10, 70, 100, 30), "Feed request" ) )
		{
			Facebook.uiFeedRequest( "http://www.studioevil.com", 
			                        "http://www.studioevil.com/img/se_logo.png", 
			                        "Studio Evil - Unity3d Facebook integration",
			                        "Unity3d Facebook integration package",
			                       	"Unity3d Facebook integration package for webplayer only. Use facebook API from your unity project!" );
		}

		if( GUI.Button( new Rect( 10, 100, 100, 30), "User info" ) )
		{
			Facebook.graphRequest( Facebook.user_me );
		}
	}
	#endregion
}
