using UnityEngine;
using LitJson;
using System.Collections;

/// <summary>
/// Facebook integration class for Unity3d.
/// 
/// Autor: Studio Evil SRL - http://www.studioevil.com
/// Basic Usage: Drag the provided "Facebook" prefab to your scene, fill in your appID and start using it.
/// 
/// All methods in this class are static, so you can call it with Facebook.<methodname>
/// This class works only in WebPlayer. Calling it on other kind of build will result in nothing
/// 
/// Please note that all methods except "getProfilePictureURL" and "isAvtive" works asynchronously
/// so you have to bind your methods to the events provided
/// 
/// We use LitJson http://litjson.sourceforge.net/ to decode json strings sent from Javascript and JSON-js to encode 
/// from javascript object to json string. 
/// Many thanks to the autors of LitJson and JSON-js
/// 
/// Please refer to http://developers.facebook.com for Facebook API Documentation
/// 
/// </summary>
public class Facebook : MonoBehaviour 
{
	#region PUBLIC_VARS
	/// <summary>
	/// file containing the basic javascript/facebook integration API
	/// </summary>
	public TextAsset jsFile;
	/// <summary>
	/// file containing the JSON-js library from Douglas Crockford
	/// </summary>
	public TextAsset jsJson;
	/// <summary>
	/// Your Facebook application ID
	/// </summary>
	public string applicationID;
	/// <summary>
	/// Facebook access token
	/// </summary>
	public static string accessToken;
	#endregion
	
	#region DELEGATES
	/// <summary>
	/// Standard delegate function used for all the events
	/// </summary>
	public delegate void FacebookEventDelegate( bool _success, JsonData _response );
	#endregion
	
	#region EVENTS
	/// <summary>
	/// Fired in response of login (both failed/successful)
	/// </summary>
	public static event FacebookEventDelegate onLogin;
	/// <summary>
	/// Fired in response of logout (both failed/successful)
	/// </summary>
	public static event FacebookEventDelegate onLogout;
	/// <summary>
	/// Fired in response of getLoginStatus (both failed/successful)
	/// </summary>
	public static event FacebookEventDelegate onGetLoginStatus;
	/// <summary>
	/// Fired in response of postFeed (both failed/successful)
	/// </summary>
	public static event FacebookEventDelegate onPostFeed;
	/// <summary>
	/// Fired in response of uiAppRequest (both failed/successful)
	/// </summary>
	public static event FacebookEventDelegate onAppRequest;
	/// <summary>
	/// Fired in response of uiFeedRequest (both failed/successful)
	/// </summary>	
	public static event FacebookEventDelegate onFeedRequest;
	/// <summary>
	/// Fired in response of all methods that uses graph API (both failed/successful)
	/// </summary>	
	public static event FacebookEventDelegate onGraphRequest;
	#endregion
	
	#region CONSTANTS
	public const string user_me 		= "/me";
	public const string user_platform 	= "/platform";
	
	public const string pictureSize_square 	= "square";
	public const string pictureSize_small 	= "small";
	public const string pictureSize_normal 	= "normal";
	public const string pictureSize_large 	= "large";
	
	public const string graphRequest_accounts 			= "/accounts";
	public const string graphRequest_activities 		= "/activities";	
	public const string graphRequest_adaccounts 		= "/adaccounts";
	public const string graphRequest_albums 			= "/albums";
	public const string graphRequest_apprequests 		= "/apprequests";
	public const string graphRequest_books 				= "/books";
	public const string graphRequest_checkins 			= "/checkins";
	public const string graphRequest_cover 				= "/cover";
	public const string graphRequest_events 			= "/events";
	public const string graphRequest_family 			= "/family";
	public const string graphRequest_feed 				= "/feed";
	public const string graphRequest_friendlists 		= "/friendlists";
	public const string graphRequest_friendrequests 	= "/friendrequests";
	public const string graphRequest_friends 			= "/friends";
	public const string graphRequest_games 				= "/games";
	public const string graphRequest_groups 			= "/groups";
	public const string graphRequest_home 				= "/home";
	public const string graphRequest_inbox 				= "/inbox";
	public const string graphRequest_interests 			= "/interests";
	public const string graphRequest_likes 				= "/likes";
	public const string graphRequest_links 				= "/links";
	public const string graphRequest_movies 			= "/movies";
	public const string graphRequest_music 				= "/music";
	public const string graphRequest_notes 				= "/notes";
	public const string graphRequest_outbox 			= "/outbox";
	public const string graphRequest_payments 			= "/payments";
	public const string graphRequest_permissions 		= "/permissions";
	public const string graphRequest_photos 			= "/photos";
	public const string graphRequest_picture 			= "/picture";
	public const string graphRequest_posts 				= "/posts";
	public const string graphRequest_statuses 			= "/statuses";
	public const string graphRequest_tagged 			= "/tagged";
	public const string graphRequest_television 		= "/television";
	public const string graphRequest_updates 			= "/updates";
	public const string graphRequest_videos 			= "/videos";
	#endregion
	
	#region INIT
	void Start () 
	{
	#if UNITY_WEBPLAYER	
		Application.ExternalEval( jsJson.text );
		Application.ExternalEval( jsFile.text.Replace( "%applicationID%", applicationID ) );
	#endif
	}
	#endregion
	
	#region HELPER_METHODS
	/// <summary>
	/// Returns the URL of the profile picture of a user. Default size is large.
	/// </summary>
	/// <param name="_user">
	/// A <see cref="System.String"/> containing the userID 
	/// </param>
	/// <returns>
	/// A <see cref="System.String"/> containing the URL of a picture
	/// </returns>
	public static string getProfilePictureURL( string _user )
	{
		return getProfilePictureURL( _user, pictureSize_large );
	}
	
	/// <summary>
	/// Returns the URL of the profile picture of a user
	/// </summary>
	/// <param name="_user">
	/// A <see cref="System.String"/> containing the userID 
	/// </param>
	/// <param name="_type">
	/// A <see cref="System.String"/> containing the picture size code. Check pictureSize_<something> constants
	/// </param>
	/// <returns>
	/// A <see cref="System.String"/> containing the URL of a picture
	/// </returns>
	public static string getProfilePictureURL( string _user, string _type )
	{
		return "https://graph.facebook.com"+_user+"/picture?type="+_type+"&access_token="+accessToken;
	}

	/// <summary>
	/// Checks if Facebook integration is active for this build/platform
	/// </summary>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public static bool isActive()
	{
		#if UNITY_WEBPLAYER
		return true;
		#endif
		return false;
	}
	#endregion
	
	#region FACEBOOK_METHODS
	/// <summary>
	/// Generic get Graph API request for user. Fires a onGraphRequest event returning the requested data
	/// </summary>
	/// <param name="_user">
	/// A <see cref="System.String"/> containing the user ID. Some constants are provided. ( user_<something> )
	/// </param>
	/// <param name="_request">
	/// A <see cref="System.String"/> containing the type of request. Constants are provided for most used types. ( grapRequest_<something> )
	/// </param>
	/// 
	/// Example:
	/// 
	/// Facebook.userGraphRequest( Facebook.user_me, Facebook.graphRequest_Feed );
	public static void userGraphRequest( string _user, string _request )
	{
		graphRequest( _user + _request );
	}
	
	/// <summary>
	/// Generic purpose get Graph API request.
	/// </summary>
	/// <param name="_request">
	/// A <see cref="System.String"/> containing the request ( "/me/feed" for example. Please refer to Facebook Graph API documentation)
	/// </param>
	public static void graphRequest( string _request )
	{
		Application.ExternalCall( "graphRequest", _request );
	}	
	
	/// <summary>
	/// Triggers a Facebook UI App Request. It opens up a popup frame asking to select users to receive the request.
	/// </summary>
	/// <param name="title">
	/// A <see cref="System.String"/> containing the title of the request
	/// </param>
	/// <param name="message">
	/// A <see cref="System.String"/> containing a small message for the user
	/// </param>
	public static void uiAppRequest( string title, string message )
	{
		Application.ExternalCall( "uiAppRequest", title,  message );
	}
	
	/// <summary>
	/// Triggers a Facebook UI Feed Request. It opens un a popup asking to post a message to current user feed.
	/// </summary>
	/// <param name="link">
	/// A <see cref="System.String"/> containing the link for the post
	/// </param>
	/// <param name="picture">
	/// A <see cref="System.String"/> containing the URL of the picture related to the post
	/// </param>
	/// <param name="name">
	/// A <see cref="System.String"/> containing the name of the link
	/// </param>
	/// <param name="caption">
	/// A <see cref="System.String"/> containing a caption for the post
	/// </param>
	/// <param name="description">
	/// A <see cref="System.String"/> containing a brief decription of the post
	/// </param>
	public static void uiFeedRequest( string link, string picture, string name, string caption, string description )
	{
		Application.ExternalCall( "uiFeedRequest", link, picture, name, caption, description );
	}
	
	/// <summary>
	/// Logs the user in, if unlogged, or ask user for privileges. NOTE: it opens up a popup window, could be blocked by the browser.
	/// </summary>
	/// <param name="scope">
	/// A <see cref="System.String"/> string of comma separated privileges the user must agree. Refer to Facebook Javascript API documentation
	/// </param>
	public static void login( string scope )
	{
		Application.ExternalCall( "login", scope );
	}
	
	/// <summary>
	/// Logs the user out
	/// </summary>
	public static void logout( )
	{
		Application.ExternalCall( "logout" );
	}
	
	/// <summary>
	/// Gets the login status for the user. Response is given by triggering the onGetLoginStatus event.
	/// </summary>
	public static void getLoginStatus( )
	{
		Application.ExternalCall( "getLoginStatus" );
	}
	
	/// <summary>
	/// Post a message to the user feed without opening a popup
	/// </summary>
	/// <param name="message">
	/// A <see cref="System.String"/> containing the message
	/// </param>
	/// <param name="picture">
	/// A <see cref="System.String"/> containing the url of a picture you want to post
	/// </param>
	/// <param name="link">
	/// A <see cref="System.String"/> containing a link related to the post
	/// </param>
	public static void postFeed( string message, string picture, string link )
	{
		Application.ExternalCall( "postFeed", message, picture, link );
	}
	#endregion
	
	#region EVENTS_HANDLING		
	void setToken( string token )
	{
		accessToken = token;
	}
	
	void onGraphRequestSuccessful( string message )
	{
		if( onGraphRequest != null )
			onGraphRequest( true, JsonMapper.ToObject (message) );		
	}
	
	void onGraphRequestFail( string message )
	{
		if( onGraphRequest != null )
			onGraphRequest( false, JsonMapper.ToObject (message) );		
	}
	
	void onLoginSuccessful( string message )
	{
		if( onLogin != null )
			onLogin( true, JsonMapper.ToObject (message) );
	}
	
	void onLoginFail( string message )
	{
		if( onLogin != null )
			onLogin( false, JsonMapper.ToObject (message) );
	}
	
	void onLogoutSuccessful( string message )
	{
		if( onLogout != null )
			onLogout( true, JsonMapper.ToObject (message) );
	}
	
	void onLogoutFail( string message )
	{
		if( onLogout != null )
			onLogout( false, JsonMapper.ToObject (message) );
	}
	
	void onGetLoginStatusSuccessful( string message )
	{
		if( onGetLoginStatus != null )
			onGetLoginStatus( true, JsonMapper.ToObject (message) );
	}
	
	void onGetLoginStatusFail( string message )
	{
		if( onGetLoginStatus != null )
			onGetLoginStatus( false, JsonMapper.ToObject (message) );
	}
	
	void onUiAppRequestSuccessful( string message )
	{
		if( onAppRequest != null )
			onAppRequest( true, JsonMapper.ToObject (message) );
	}
	
	void onUiAppRequestFail( string message )
	{
		if( onAppRequest != null )
			onAppRequest( false, JsonMapper.ToObject (message) );
	}

	void onUiFeedRequestSuccessful( string message )
	{
		if( onFeedRequest != null )
			onFeedRequest( true, JsonMapper.ToObject (message) );
	}
	
	void onUiFeedRequestFail( string message )
	{
		if( onFeedRequest != null )
			onFeedRequest( false, JsonMapper.ToObject (message) );
	}

	void onPostFeedSuccessful( string message )
	{
		if( onPostFeed != null )
			onPostFeed( true, JsonMapper.ToObject (message) );
	}
	
	void onPostFeedFail( string message )
	{
		if( onFeedRequest != null )
			onFeedRequest( false, JsonMapper.ToObject (message) );
	}
	#endregion	
}
