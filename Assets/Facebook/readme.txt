Autor: Studio Evil
Facebook API Integration for Unity 3.4/Web player

This package lets you integrate your web unity project with Facebook in less than 5 minutes.

Release: 1.0
Release Date: 09/12/2011

Requirements:
	Unity 3.x

Why:
	This integration package works with facebook javascript API, without any need of server-side scripting	

Quick Start:
	1) Create a facebook application profile using http://developers.facebook.com
	2) Create a new unity project
	3) Switch platform to "Web Player" (Unity3d->Files->Build settings)
	4) Import the package
	5) Open the test scene
	6) Put your Facebook Application ID in the related field on the "Facebook" game object on the scene.
	7) Buil the project and upload it on your online test server	
	
	
How it works:
	The package works by injecting some javascript code directly from the unity project, so there is no need to
	change the template or work with html. All the work is done by calling directly the official Facebook Javascript
	API. 
	Every operation is asynchronous, so you get responses by registering your event handlers
	
	There are some basic types of functions, all static and accessible with "Facebook.<functionname>":
	
	- Login/Authorize functions
		
		login()
		logs the user in, requesting some permissions or request only permissions if the user is already logged.
		fires onLogin event
		for more informations about the permissions to ask, please refer to: 
			http://developers.facebook.com/docs/reference/api/permissions/
		
		logout()
		logs the user out, fires onLogout event
		
		getLoginStatus()
		gets the current login status of the user. 
		fires onGetLoginStatus event
		
	- UI functions: functions that works opening a facebook popup request
		
		uiAppRequest()
		opens up a popup request that lets you select friends to share your app
		fires onAppRequest event
		
		uiFeedRequest()
		opens up a popup request asking the user to post a feed on his/her wall
		fires onFeedRequest event
		
	- Direct functions
		
		postFeed()
		post a feed, similar to uiFeedRequest, but without asking permissions to the user
		fires onPostFeed event
		
	- Graph API functions, get: used to get infos about the user and friends, pages and other facebook objects
		refer to http://developers.facebook.com/docs/reference/api/ for an exhaustive documentation,
		http://developers.facebook.com/tools/explorer for an interactive explorer for the Graph API
		
		userGraphRequest()
		gets infos about a user
		fires onGraphRequest event
		
		
		graphRequest()
		generic Graph API request
		fires onGraphRequest event
		
	- Helper functions
		
		getProfilePictureURL()
		gets the user profile picture, default large size
	
Event handling:
	
	All events are handled using standard c# event system. The event function called is always
	public delegate void FacebookEventDelegate( bool _success, JsonData _response )
	where _success indicates the success of the operation and _response is the response object, containing the 
	informations requested or some infos about errors.
	
	Please refer to LitJson documentation for the JsonData object

Basic Usage:
	To use this package you need to create a Facebook app profile using http://developers.facebook.com
	Open up a new Unity project, switch to "web player" platform, import the package and drag the "Facebook" prefab
	on the scene. 
	Fill in the Facebook Application ID in the corresponding field on Facebook GameObject then start to code.
	
	Please note that this package works in the web player only and Facebook Web applications are required to be 
	hosted in a public web server.
			
Example code:

	logs the user in, requesting some useful permissions
			Facebook.login( "email,publish_actions,publish_stream,read_friendlists" );

	requests my friendslist (user must be logged and have needed permissions) and fires a onGraphRequest event 
	when done
			Facebook.userGraphRequest( Facebook.user_me, Facebook.graphRequest_friendlists );
	
	get the current user pic url, normal size
			Facebook.getProfilePictureURL( Facebook.user_me, Facebook.pictureSize_normal )
	

Note about the user/page parameter:
	
	Some functions requires the _user param.
	We provided the Facebook.user_me constant to identify the currently logged user.
	You can also use a string in the format "/userid" to get informations about <userid> user, if authorized.
	If for example a friend of mine has the "svicolone" userid and I have requested the needed permissions I can
	fetch svicolone's data using
			Facebook.graphRequest( "/svicolone" );
	that fires a onGraphRequest event with svicolone's data in _response parameter
	
	I can also ask Facebook svicolone's friends list using
			Facebook.userGraphRequest( "/svicolone", Facebook.graphRequest_friendlists );
	The response will come with a onUserGraphRequest event fired.		
	

For support, assistance and bug reports please write to support_assetstore@studioevil.com
			
3rd party libraries included:
	LitJson - http://litjson.sourceforge.net/
	JSon-js - http://www.json.org/js.html
	